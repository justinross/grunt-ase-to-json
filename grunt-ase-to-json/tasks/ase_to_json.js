/*
 * grunt-ase-to-json
 * 
 *
 * Copyright (c) 2014 Justin Ross
 * Licensed under the MIT license.
 */

'use strict';

module.exports = function(grunt) {

    // Please see the Grunt documentation for more information regarding task
    // creation: http://gruntjs.com/creating-tasks

    grunt.registerMultiTask('ase_to_json', 'Easily convert .ase (Adobe Swatch Exchange) files from Adobe Kuler to json for importing into Stylus stylesheets', function() {
        var aseJSON = require('adobe-swatch-exchange');
        // Merge task-specific and/or target-specific options with these defaults.
        var options = this.options({

            punctuation: '.',
            separator: ', '
        });

        // Iterate over all specified file groups.
        this.files.forEach(function(f) {
            
            var src = grunt.file.read(f.src, {encoding: null});
            var json = aseJSON.decode(src, "stylus");
            //grunt.log.writeln(json);

            // Write the destination file.
            grunt.file.write(f.dest, json);

            // Print a success message.
            grunt.log.writeln('File "' + f.dest + '" created.');
        });
    });

};

//Load an .ase file
//Convert it to JSON
//save the JSON file with the same name
//Bam says the lady
